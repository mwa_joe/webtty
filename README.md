#WebTTY = Web + TTY

WebTTY is a Fork of the [wetty](https://github.com/krishnasrinivas/wetty) project 
by Srinivas Krishna

Terminal over HTTP and HTTPS. WebTTY is an alternative
 to ajaxterm/anyterm but much better than them 
because it uses ChromeOS' terminal emulator 
[hterm](https://chromium.googlesource.com/apps/libapps/+/master/hterm/) 
which is a full fledged implementation of
terminal emulation written entirely in Javascript. 
Also it uses websockets instead of Ajax and hence better response time.

![Webtty](/terminal.png?raw=true)

# INSTALLATION
This WEBTTY component is bundled for Linux & OSX by the [Postgres by BigSQL](http://bigsql.org) project and is controlled by the [PGCLI (command line package manager)](https://www.bigsql.org/package-manager.jsp).   It provides a robust, simple & secure facility for providing browser based Terminal access to a remote SSH Server.


# BUILD FROM SCRATCH

##Pre-Req's

###CentOS/Redhat:
    yum install git gcc npm nodejs

###OSX :
    Use One-click NodeJS installer v6.9.x LTS from http://nodejs.org

    This is experimental, it seems to work with the following caveats:
        1.) search-n-replace "bin/login" w/ "usr/bin/login" in app_webtty.js
        2.) run with SUDO


## Get the Source and Build it
*  `git clone https://bitbucket.org/openscg/webtty.git`

*  `cd webtty`

*  `npm install`

Run on HTTP:
-----------

    node app_webtty.js -p 3000

If you run it as root it will launch `/bin/login` (where you can specify
the user name), else it will launch `ssh` and connect by default to
`localhost`.

If instead you wish to connect to a remote host you can specify the
`--sshhost` option, the SSH port using the `--sshport` option and the
SSH user using the `--sshuser` option.

You can also specify the SSH user name in the address bar like this:

  `https://yourserver:3000/webtty/ssh/<username>`

Run on HTTPS:
------------

Always use HTTPS! If you don't have SSL certificates from a CA you can
create a self signed certificate using this command:

  `openssl req -x509 -newkey rsa:2048 -keyout key.pem -out cert.pem -days 30000 -nodes`

And then run:

    node app_webtty.js --sslkey key.pem --sslcert cert.pem -p 3000

Again, if you run it as root it will launch `/bin/login`, else it will
launch SSH to `localhost` or a specified host as explained above.


Run webtty as a service daemon
-----------------------------

Install webtty globally with -g option:

```bash
    $ sudo npm install webtty -g
    $ sudo cp /usr/local/lib/node_modules/webtty/bin/webtty.conf /etc/init
    $ sudo start webtty
```

This will start webtty on port 3000. If you want to change the port or redirect stdout/stderr you should change the last line in `webtty.conf` file, something like this:

    exec sudo -u root webtty -p 80 >> /var/log/webtty.log 2>&1
